angular.module('starter.controllers', [])

.controller('DashCtrl', ['$scope','$http', function($scope,$http) {
  $scope.dataTrick = {
    id: "001",
    color: "contenido prueba",
    tel: "938329"
  };
}])

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, socket) {
  $scope.chat = Chats.get($stateParams.chatId);
  $scope.items = [];//tirar de la base de datos y cargar el valor
  $scope.add = function(item) {
    var sendText = item;
    console.log($scope.items)
    socket.emit('chat message',sendText);
  };
  socket.on('chat message', function(msg) {
    console.log("REcibo");
    $scope.items.push(msg);
    console.log($scope.items);
  })
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});